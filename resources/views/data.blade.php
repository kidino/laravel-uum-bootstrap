@extends('layouts.dashboard')

@section('content') 

<div class="col-12 justify-content-between d-flex">
                <h2>Senarai Pendaftaran</h2>
                <a class="btn btn-primary btn-sm align-self-start" href="/edit">Add New</a>
            </div>


            <div class="col-12 bg-white rounded shadow mt-3">
                <table class="table table-striped">

                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Telefon</th>
                            <th>Negeri</th>
                            <th>Jawatan</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-secondary">Inactive</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Iszuddin</td>
                            <td>iszuddin@domain.com</td>
                            <td>011-2222 3333</td>
                            <td>Kuala Lumpur</td>
                            <td>Bos</td>
                            <td><span class="badge bg-success">Active</span></td>
                            <td>
                                <a href="/edit">Edit</a>
                                <a href="#" class="text-danger">Delete</a>
                            </td>
                        </tr>
                    </tbody>

                </table>

                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                </nav>



            </div>

@endsection 